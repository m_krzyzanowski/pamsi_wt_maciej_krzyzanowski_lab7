#include <iostream>

using namespace std;

class Drzewo;

class ElementDrzewa
{
private:
    int wartosc;
    ElementDrzewa *ojciec;
    ElementDrzewa *lewy;
    ElementDrzewa *prawy;
    friend class Drzewo;

public:
    ElementDrzewa()
    {
        wartosc = 0;
        ojciec = NULL;
        lewy = NULL;
        prawy = NULL;

    }
};


class Drzewo
{
private:
    ElementDrzewa *korzen;

// metody pomocnicze, prywatne:
    ElementDrzewa* addHelp(int,ElementDrzewa*&); //IN: wartosc ktora chemy dodac, korzen lub wezel drzewa OUT: wskaznik na dodany element
    ElementDrzewa* removeHelp(int,ElementDrzewa*&); //IN: wartosc ktora chemy usunac, korzen lub wezel drzewa OUT: wskaznik na usuniety wezel
    int heightHelp(ElementDrzewa*); //IN: wezel drzewa OUT: wysokosc drzewa (metoda zwraca wysokos drzewa w podanym wezle)
    int heightDifference(ElementDrzewa*); //IN: wezel drzewa OUT: roznica wysokosci (metoda zwraca roznice wysokosci miedzy prawym i lewy poddrzewem)

    /* Rotacje 4 typow, zaleznych od rodzaju polaczenia pomiedzy wezlami przed rotaja */
    ElementDrzewa* RR_Rotation(ElementDrzewa*);
    ElementDrzewa* LL_Rotation(ElementDrzewa*);
    ElementDrzewa* RL_Rotation(ElementDrzewa*);
    ElementDrzewa* LR_Rotation(ElementDrzewa*);
    ElementDrzewa* restructure(ElementDrzewa*); //metoda przywraca wlasciwosc drzewa AVL

    void printPreOrderHelp(ElementDrzewa*); //IN: wezel drzewa, ktore chemy wyswietlic (metoda wyswietla przejscie PreOrder)
    void printInOrderHelp(ElementDrzewa*);//IN: wezel drzewa, ktore chemy wyswietlic (metoda wyswietla przejscie InOrder)
    void printPostOrderHelp(ElementDrzewa*); //IN: wezel drzewa, ktore chemy wyswietlic (metoda wyswietla przejscie PostOrder)

public:

//metody publiczne:
    Drzewo() {korzen=NULL;}
    ~Drzewo() {delete korzen;}

    ElementDrzewa* add(int); //IN: wartosc elementu, ktory chcemy dodac (metoda dodaje element do drzewa)
    ElementDrzewa* remove(int); //IN: wartosc elementu, ktory chcemy usunac (metoda usuwa wybrany element z drzewa)
    int height(); //metoda zwraca wysokosc drzewa
    void printInRoot(int); //IN: wezel, ktory chemy wuswietlic (wyswietla wartosc ojca, potomkow, wysokosc lewego i prawego poddrzewa, roznice wysokosci poddrze)
    void printPreOrder(); //przejscie pre-order
    void printInOrder(); //przejscie in-order
    void printPostOrder(); //przejscie post-order
};


///////////////////////////////////// metody ////////////////////////////////////////////

ElementDrzewa *Drzewo::addHelp(int wart, ElementDrzewa* &wsk)
{
    if(wsk == NULL) //sprawdza czy korzen jest pusty
    {
        wsk = new ElementDrzewa;
        wsk->wartosc = wart;
        return wsk;
    }
    else if (wart < wsk->wartosc) //jezeli wartosc, ktora chcemy dodac jest wiekasza
    {
        wsk->lewy = addHelp(wart,wsk->lewy);
        wsk = restructure(wsk);
    }
    else if(wart > wsk->wartosc) //jezeli wartosc ktora chcemy dodac jest mniejsza
    {
        wsk->prawy = addHelp(wart,wsk->prawy);
        wsk = restructure(wsk);
    }
    return wsk;
}

ElementDrzewa *Drzewo::add(int wart)
{
    return addHelp(wart,korzen);
}

///////////////////////////////////////////////////////////////////////////////////////

ElementDrzewa* Drzewo::removeHelp(int wart,ElementDrzewa* &wsk)
{
    if (wsk == NULL) return wsk; //warunek podstawowy rekurencji
	if (wart < wsk->wartosc) //jezeli podana wartosc jest mnijesza, schodzimy w drzewo w lewa strone
    {
		wsk->lewy = removeHelp(wart,wsk->lewy);
	}
	else if (wart > wsk->wartosc) //jezeli podana wartosc jest wiksza, schodzimy w drzewo w prawa strone
    {
		wsk->prawy = removeHelp(wart,wsk->prawy);
	}
	else //jezeli podana wartosc jest rowna wartosci wezla
    {
        if (wsk->lewy == NULL) //jezeli ElementDrzewa nie ma prawego potomka
        {
			ElementDrzewa *delWsk;
			delWsk = wsk->prawy;
			delete wsk;
			return delWsk;
		}
		else if (wsk->prawy == NULL) //jezeli element drzewa ma lewego potomka
        {
			ElementDrzewa *delWsk;
			delWsk = wsk->lewy;
			delete wsk;
			return delWsk;
		}
		else
        {
			ElementDrzewa *minWsk = wsk->prawy;
			while(minWsk->lewy != NULL) minWsk = minWsk->lewy; //znajdujemy wskaznik na najmniejsza wartosc

			wsk->wartosc = minWsk->wartosc;
			wsk->prawy = removeHelp(minWsk->wartosc,wsk->prawy);
		}
	}

	if(wsk == NULL) return wsk;

	wsk = restructure(wsk); //przywracanie wlasciwosci drzewa AVL

    return wsk;
}

ElementDrzewa* Drzewo::remove(int wart)
{
    return removeHelp(wart,korzen);
}

///////////////////////////////////////////////////////////////////////////////////////

int Drzewo::height()
{
    return heightHelp(korzen);
}

int Drzewo::heightHelp(ElementDrzewa* wsk)
{
    if (wsk == NULL) return 0;
    else return max(heightHelp(wsk->lewy), heightHelp(wsk->prawy)) + 1;
}

///////////////////////////////////////////////////////////////////////////////////////

int Drzewo::heightDifference(ElementDrzewa* wsk)
{
    int l_height = heightHelp(wsk->lewy);
    int r_height = heightHelp(wsk->prawy);
    return (l_height - r_height);
}

///////////////////////////////////////////////////////////////////////////////////////


//Rotacja RR
ElementDrzewa *Drzewo::RR_Rotation(ElementDrzewa *wsk)
{
    ElementDrzewa *wsk_pomoc;
    wsk_pomoc = wsk->prawy;
    wsk->prawy = wsk_pomoc->lewy;
    wsk_pomoc->lewy = wsk;
    cout << "Rotacja RR." << " ";
    return wsk_pomoc;
}

//Rotacja LL
ElementDrzewa *Drzewo::LL_Rotation(ElementDrzewa *wsk)
{
    ElementDrzewa *wsk_pomoc;
    wsk_pomoc = wsk->lewy;
    wsk->lewy = wsk_pomoc->prawy;
    wsk_pomoc->prawy = wsk;
    cout << "Rotacja LL." << " ";
    return wsk_pomoc;
}

//Rotacja LR
ElementDrzewa *Drzewo::LR_Rotation(ElementDrzewa *wsk)
{
    ElementDrzewa *wsk_pomoc;
    wsk_pomoc = wsk->lewy;
    wsk->lewy = RR_Rotation(wsk_pomoc);
    cout << "Rotacja LR." << " ";
    return LL_Rotation(wsk);
}

//Rotacja RL
ElementDrzewa *Drzewo::RL_Rotation(ElementDrzewa *wsk)
{
    ElementDrzewa *wsk_pomoc;
    wsk_pomoc = wsk->prawy;
    wsk->prawy = LL_Rotation(wsk_pomoc);
    cout << "Rotacja RL." << " ";
    return RR_Rotation(wsk);
}

// Zbalansowanie drzewa
ElementDrzewa *Drzewo::restructure(ElementDrzewa *wsk)
{
    int balance = heightDifference(wsk);
    if (balance > 1)
    {
        if (heightDifference(wsk->lewy) > 0)
            wsk = LL_Rotation(wsk);
        else
            wsk = LR_Rotation(wsk);
    }
    else if (balance < -1)
    {
        if (heightDifference(wsk->prawy) > 0)
            wsk = RL_Rotation(wsk);
        else
            wsk = RR_Rotation(wsk);
    }

    return wsk;
}

///////////////////////////////////////////////////////////////////////////////////////

void Drzewo::printInRoot(int wart)
{
    ElementDrzewa* wsk = korzen;
    while(wsk != NULL && wsk->wartosc != wart) //petla znajduje i ustawia wskaznik na ElementDrzewa o podanej wartosci
    {
            if(wart < wsk->wartosc) wsk=wsk->lewy;
            else wsk=wsk->prawy;
    }

    if(wsk != NULL)
    {
        cout << "Ojciec: " << wsk->wartosc << endl;
        if(wsk->lewy == NULL) cout << "Lewy syn = NULL" << endl;
        else cout << "Lewy syn = " << wsk->lewy->wartosc << endl;
        if(wsk->prawy == NULL) cout << "Prawy syn = NULL" << endl;
        else cout << "Prawy syn = " << wsk->prawy->wartosc << endl;
        cout << "Wysokosc lewego poddrzewa: " << heightHelp(wsk->lewy) << endl;
        cout << "Wysokosc prawego poddrzewa: " << heightHelp(wsk->prawy) << endl;
        cout << "Roznica wysokosci: " << heightDifference(wsk) << endl << endl;
    }
    else
    {
        cout << "Wartosc " << wart << " nie wystepuje w drzewie." << endl;
    }
}

///////////////////////////////////////////////////////////////////////////////////////

void Drzewo::printInOrderHelp(ElementDrzewa* wsk)
{
    if(wsk != NULL)
    {
        if(wsk->lewy != NULL) printInOrderHelp(wsk->lewy);
        cout << wsk->wartosc << " ";
        if(wsk->prawy != NULL) printInOrderHelp(wsk->prawy);
    }
    else cout << "Drzewo jest puste." << endl;
}

void Drzewo::printInOrder()
{
    printInOrderHelp(korzen);
}

///////////////////////////////////////////////////////////////////////////////////////

void Drzewo::printPreOrderHelp(ElementDrzewa* wsk)
{
    if(wsk != NULL)
    {
        cout << wsk->wartosc << " ";
        if(wsk->lewy != NULL) printPreOrderHelp(wsk->lewy);
        if(wsk->prawy != NULL) printPreOrderHelp(wsk->prawy);
    }
    else cout << "Drzewo jest puste." << endl;
}

void Drzewo::printPreOrder()
{
    printPreOrderHelp(korzen);
}

///////////////////////////////////////////////////////////////////////////////////////

void Drzewo::printPostOrderHelp(ElementDrzewa* wsk)
{
    if(wsk != NULL)
    {
        if(wsk->lewy != NULL) printPostOrderHelp(wsk->lewy);
        if(wsk->prawy != NULL) printPostOrderHelp(wsk->prawy);
        cout << wsk->wartosc << " ";
    }
    else cout << "Drzewo jest puste." << endl;
}

void Drzewo::printPostOrder()
{
    printPostOrderHelp(korzen);
}

