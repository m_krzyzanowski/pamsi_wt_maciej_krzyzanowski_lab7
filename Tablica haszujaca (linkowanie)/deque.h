#include <iostream>
#include <exception>


using namespace std;

class DequeEmptyException;
class SizeOfDequeException;

template <typename T> class Deque;

template <typename T>
class Wezel
{
private:
	T wartosc;
	Wezel * nastepny;
	Wezel * poprzedni;
	friend class Deque<T>;

public:

    /******************metody dostepu do zmiennych*******************/


    T getWartosc() const {return wartosc;} //metoda dostapu do wartosci
    Wezel* getNastepny() const {return nastepny;} //metoda dostepu do nastepnego
    Wezel* getPoprzedni() const {return poprzedni;} //metoda dostepu do poprzedniego
    void setWartosc(T nowyW) {wartosc = nowyW;} //metoda modyfikujaca wartosc
    void setNastepny(Wezel<T> *nowyN) {nastepny = nowyN;} //metoda modyfikuja nastepny
    void setPoprzedni(Wezel<T> *nowyP) {poprzedni = nowyP;} //metoda modyfikuja poprzedni

    Wezel()
	{
		nastepny = NULL;
		poprzedni = NULL;
	}
};

template <typename T>
class Deque
{
private:
    Wezel <T> *glowa;
    Wezel <T> *ogon;
    int rozmiar;

public:

    /******************metody dostepu do zmiennych*******************/

    Wezel<T>* getGlowa() const {return glowa;} //metoda dostepu do glowy
    Wezel<T>* getOgon() const {return ogon;} //metoda dostepu do ogona
    int getRozmiar() const {return rozmiar;} //metoda dostepu do rozmiaru
    void setGlowa(Wezel<T> *nowyG) {glowa = nowyG;} //metoda modyfikujaca glowa
    void setOgon(Wezel<T> *nowyO) {ogon = nowyO;} //metoda modyfikujaca ogon
    void setRozmiar(int r) {rozmiar = r;} //metoda modyfikujaca liczby elementow

    int size() const; //Zwraca ilo�� obiekt�w przechowywanych w deque
    bool isEmpty() const; //Zwraca true je�li deque jest pusty
    T & front() const; // Zwraca pierwszy obiekt w deque. (Wyrzuca DequeEmptyException je�li deque jest pusta)
    T & back() const; // Zwraca ostatni obiekt w deque. (Wyrzuca DequeEmptyException je�li deque jest pusta)
    void addFront(const T&); //Dodaje obiekt do poczatku deque�a. IN: element, ktory chcemy wstawic
    Deque <T> & removeFront(); // Usuwa pierwszy obiekt z deque. (Wyrzuca DequeEmptyException je�li deque jest pusta)
    void addBack(const T&); //Dodaje obiekt na ko�cu deque�a. IN: element, ktory chcemy wstawic
    Deque <T> & removeBack(); //Usuwa ostatni obiekt z deque. (Wyrzuca DequeEmptyException je�li deque jest pusta)
    void insert(int,T); //Dodaje element w miejsce o podanym indeksie i wartosci.
    void erase(int); //Usuwa element o podanym indeksie. IN: indeks elementu ktory chemu usunac
    void clear(); //Usuwa wszystkie elementy
    T operator [] (int); //zwraca element listy o podanym indeksie (Wyrzuca SizeOfListException je�li deque jest pusta)
    void show(); //wyswietla kolejke


    Deque() //konstruktor
    {
        glowa = NULL;
        ogon = NULL;
        rozmiar = 0;
    }
    ~Deque() //destruktor
    {
        clear();
    }
};

template <typename T>
int Deque<T>::size() const
{
    return rozmiar;
}

template <typename T>
bool Deque<T>::isEmpty() const
{
    if(rozmiar==0) return true;
    else return false;
}

template <typename T>
T & Deque<T>::front() const
{
    if(rozmiar==0) throw DequeEmptyException();
    else return glowa->wartosc;
}

template <typename T>
T & Deque<T>::back() const
{
    if(rozmiar==0) throw DequeEmptyException();
    else return ogon->wartosc;
}

template <typename T>
void Deque<T>::addFront(const T & elem)
{
	Wezel <T> *nowy = new Wezel <T>;
    nowy->wartosc = elem; //ustawia wartosc kontenera

    if(rozmiar==0)
    {
        glowa = nowy;
        ogon = nowy;
    }
    else
    {
        nowy->nastepny = glowa;
        glowa->poprzedni = nowy;
        glowa = nowy;
    }
    rozmiar++;
}

template <typename T>
Deque<T> & Deque<T>::removeFront()
{
    Wezel <T> *wsk = glowa;
    if(rozmiar==0) throw DequeEmptyException();
    else
    {
        glowa = glowa->nastepny;
        delete wsk;
        --rozmiar;
        return *this;
    }
}

template <typename T>
void Deque<T>::addBack(const T & elem)
{
	Wezel <T> *nowy = new Wezel <T>;
    nowy->wartosc = elem; //ustawia wartosc kontenera

    if(rozmiar==0)
    {
        glowa=nowy;
        ogon=nowy;
    }
    else
    {
        ogon->nastepny = nowy;
        nowy->poprzedni = ogon;
        ogon = nowy;
    }
    rozmiar++;
}

template <typename T>
Deque<T> & Deque<T>::removeBack()
{
    Wezel <T> *wsk = ogon;
    if(rozmiar==0) throw DequeEmptyException();
    else
    {
        ogon = ogon->poprzedni;
        delete wsk;
        rozmiar--;
    }
    return *this;
}

template <typename T>
void Deque<T>::erase(int index)
{
    if(index >= rozmiar || index < 0) throw SizeOfDequeException();
    else if (index == 0) removeFront();
    else if (index == rozmiar-1) removeBack();
    else
    {
        Wezel<T> *wsk1 = glowa;
        Wezel<T> *wsk2 = glowa->nastepny;
        Wezel<T> *wsk3 = glowa->nastepny->nastepny;
        for(int i=1; i<index; i++)
        {
            wsk1 = wsk1->nastepny;
            wsk2 = wsk2->nastepny;
            wsk3 = wsk3->nastepny;
        }

        wsk1->nastepny = wsk3;
        wsk3->poprzedni = wsk1;
        delete wsk2;
        rozmiar--;
    }
}

template <typename T>
void Deque<T>::insert(int index, T wart)
{
    if(index >= rozmiar || index < 0) throw SizeOfDequeException();
    else if (index == 0) addFront(wart);
    else if (index == rozmiar-1) addBack(wart);
    else
    {
        Wezel<T> *wsk1 = glowa;
        Wezel<T> *wsk2 = glowa->nastepny;
        Wezel<T> *nowy = new Wezel<T>;
        for(int i=1; i<index; i++)
        {
            wsk1 = wsk1->nastepny;
            wsk2 = wsk2->nastepny;
        }

        nowy->wartosc = wart;
        nowy->poprzedni = wsk1;
        nowy->nastepny = wsk2;
        wsk1->nastepny = nowy;
        wsk2->poprzedni = nowy;

        rozmiar++;
    }
}

template <typename T>
void Deque<T>::clear()
{
    while(!isEmpty()) removeFront();
}

template <typename T>
T Deque<T>::operator[](int index)
{
    Wezel<T>* wsk = glowa;
    if(index < 0 || index >= rozmiar) throw SizeOfDequeException();
    else
    {
        for(int i=0; i<index; i++)
            wsk = wsk->nastepny;

        return wsk->wartosc;
    }
}

class DequeEmptyException : public std::exception
{
public:
	virtual const char * what() const noexcept
	{
		return "Lista jest pusta.";
	}
};

class SizeOfDequeException : public std::exception
{
public:
	virtual const char * what() const noexcept
	{
		return "Przekroczyles rozmiar tablicy.";
	}
};
